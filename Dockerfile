﻿FROM node:14.16.0 AS builder

WORKDIR /usr/src/app

COPY package.json package.json

RUN npm config set registry https://registry.npm.taobao.org

RUN npm i

COPY . .

RUN npm run build

FROM nginx

EXPOSE 80

COPY --from=builder /usr/src/app/dist/Web /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]

