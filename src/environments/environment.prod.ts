import { Environment } from '@delon/theme';

export const environment = {
  production: true,
  useHash: true,
  api: {
    baseUrl: 'http://106.52.233.174:30000/api',
    // refreshTokenEnabled: true,
    // refreshTokenType: 'auth-refresh',
  },
} as Environment;
