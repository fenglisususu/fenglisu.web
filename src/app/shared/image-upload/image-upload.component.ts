import { Component, OnInit, Input, ViewChild, forwardRef, Inject } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Observable } from 'rxjs';
import { NzUploadFile, NzUploadComponent, NzUploadChangeParam } from 'ng-zorro-antd/upload';

@Component({
  selector: 'app-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.less'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ImageUploadComponent),
      multi: true,
    },
  ],
})
export class ImageUploadComponent implements OnInit, ControlValueAccessor {
  @Input() fileList: NzUploadFile[] = [];

  /**
   * 限制数量
   *
   * @memberof ImageUploadComponent
   */
  @Input() limit = 50;
  /**
   * 限制大小(KB)
   *
   * @memberof ImageUploadComponent
   */
  @Input() size = 5120;
  @Input() disabled = false;
  @Input() type = 'default';
  @Input() fileTypes = 'image/png,image/jpeg,image/gif,image/bmp';
  @Input() allowPreview = true;

  @ViewChild('upload', { static: true }) upload: NzUploadComponent | undefined;

  previewImage: any = '';
  previewVisible = false;
  uploadUrl = '/common/upload';

  @Input() beforeRemove: (image: string | undefined) => boolean | Observable<any> = () => true;

  onChange: (value: string[] | string) => void = () => null;
  onTouch: () => void = () => null;

  get showButton(): boolean {
    return this.fileList.length < this.limit && !this.disabled;
  }

  constructor() {}

  ngOnInit() {}
  handlePreview = (file: NzUploadFile) => {
    if (!this.allowPreview) return;

    this.previewImage = file.url || file.thumbUrl;
    this.previewVisible = true;
  };
  writeValue(value: string[] | string): void {
    if (value) {
      if (typeof value === 'string') {
        this.fileList = [
          {
            id: value,
            uid: value,
            url: value,
            thumbUrl: value,
            name: value,
            size: undefined,
            type: undefined,
          },
        ];
      } else {
        this.fileList = value.map((i) => {
          return { id: i, uid: i, url: i, thumbUrl: i, name: i, type: undefined, size: undefined };
        });
      }
    } else this.fileList = [];
  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  onRemove = (file: NzUploadFile) => {
    if (this.disabled) return false;
    return this.beforeRemove(file.url);
  };

  onUploadChange(e: NzUploadChangeParam) {
    const fileList = e.fileList;
    const type = e.type;

    this.fileList = fileList.filter((item) => {
      if (item.response != null) {
        return item.response;
      }
      return true;
    });
    if (type === 'success' || type === 'removed') {
      if (this.limit === 1) {
        const file = fileList[0];
        if (file) {
          this.onChange(file.response ? file.response : file.url);
        } else {
          this.onChange('');
        }
      } else {
        this.onChange(
          fileList.map((i) => {
            if (i.response) {
              return i.response;
            } else {
              return i.name;
            }
          }),
        );
      }
    }
  }
}
