import { Injectable } from '@angular/core';
import { TestModule } from '../modules/test.module';

@Injectable({
  providedIn: 'root',
})
export class TestService {
  getMessage() {
    return 'test';
  }
}
