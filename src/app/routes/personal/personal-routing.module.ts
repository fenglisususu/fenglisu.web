import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PersonalPictureIndexComponent } from './picture/index.component';

const routes: Routes = [{ path: 'picture', component: PersonalPictureIndexComponent, data: { title: '照片墙' } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PersonalRoutingModule {}
