import { ThrowStmt } from '@angular/compiler';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { _HttpClient } from '@delon/theme';
import { NzUploadFile, NzUploadComponent, NzUploadChangeParam } from 'ng-zorro-antd/upload';

interface Image {
  index: number;
  id: string;
  image: string;
  name: string;
}

@Component({
  selector: 'app-personal-picture-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.less'],
})
export class PersonalPictureIndexComponent implements OnInit {
  userId: string = '';
  pics: NzUploadFile[] = [];
  pic: string | undefined = '';
  previewVisible = false;

  constructor(
    public http: _HttpClient,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    @Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService,
  ) {}

  ngOnInit(): void {
    this.userId = this.tokenService.get()?.id;
    this.loadUserPics();
  }

  loadUserPics() {
    this.http
      .post<string[]>('/user/GetUserPics', { userId: this.userId })
      .subscribe((res) => {
        if (res.length > 0) {
          this.pics = [
            ...res.map<NzUploadFile>(
              (v, i, arr) =>
                <NzUploadFile>{
                  url: v,
                  thumbUrl: v,
                  response: v,
                },
            ),
          ];
        }
      });
  }

  remove = (file: NzUploadFile): boolean => {
    let result = false;
    this.http.post('/user/removeuserpic', { userId: this.userId, picture: file.response }).subscribe((data) => {
      this.pics = [
        ...this.pics.filter((v, i, arr) => {
          return v.response !== file.response;
        }),
      ];
    });
    return result;
  };

  handlePreview = async (file: NzUploadFile) => {
    this.pic = file.response;
    this.previewVisible = true;
  };

  onUploadChange(e: NzUploadChangeParam) {
    const fileList = e.fileList;
    const type = e.type;

    if (type === 'success' || type === 'removed') {
      this.pics = [
        ...fileList.map<NzUploadFile>(
          (v, i, arr) =>
            <NzUploadFile>{
              url: v.response,
              thumbUrl: v.response,
              response: v.response,
            },
        ),
      ];
    }
  }
}
