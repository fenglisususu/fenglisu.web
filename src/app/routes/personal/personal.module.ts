import { NgModule, Type } from '@angular/core';
import { SharedModule } from '@shared';
import { PersonalRoutingModule } from './personal-routing.module';
import { PersonalPictureIndexComponent } from './picture/index.component';

const COMPONENTS: Type<void>[] = [PersonalPictureIndexComponent];

@NgModule({
  imports: [SharedModule, PersonalRoutingModule],
  declarations: COMPONENTS,
})
export class PersonalModule {}
